using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using TMPro;
using UnityEngine.SceneManagement;

public class WordPuzzleGame2 : MonoBehaviour
{
    [SerializeField] private TextMeshPro displayText;
    [SerializeField] private TextMeshPro word1;
    [SerializeField] private TextMeshPro word2;
    [SerializeField] private TextMeshPro word3;
    [SerializeField] private TextMeshPro word4;
    [SerializeField] private GameObject nextButton;
    [SerializeField] private GameObject endButton;
    [SerializeField] private GameObject wordPuzzle;
    private readonly List<List<string>> Puzzle = new List<List<string>>{
        new List<string>{ "What do we call the first ten amendments to the Constitution?", "Of", "Bill", "Law", "Rights", "Bill", "Of", "Rights"}, // question, options, solution
        new List<string>{ "How is one branch of government prevented from becoming too powerful?", "Balances", "Bill", "And", "Checks", "Balances", "And", "Checks"}
    };

    public static List<string> Answer = new List<string> { "", "", "" };
    private int currentQuestion = -1;

    void Start()
    {
    }

    public void nextWordPuzzle()
    {

        nextButton.SetActive(false);
        currentQuestion++;
        if (currentQuestion == 2)
        {
            displayText.text = "Congrats! You've finished the test!";
            endButton.SetActive(true);
            return;
        }
        displayText.text = Puzzle[currentQuestion][0];
        word1.text = Puzzle[currentQuestion][1];
        word2.text = Puzzle[currentQuestion][2];
        word3.text = Puzzle[currentQuestion][3];
        word4.text = Puzzle[currentQuestion][4];
        wordPuzzle.SetActive(true);
    }

    public void modifyAnswer(int index, string word)
    {
        Answer[index] = word;
        if (Answer[0] == Puzzle[currentQuestion][5] && Answer[1] == Puzzle[currentQuestion][6] && Answer[2] == Puzzle[currentQuestion][7])
        {
            wordPuzzle.SetActive(false);
            displayText.GetComponent<DisplayText>().displayNextQuestion();
        }
    }
}

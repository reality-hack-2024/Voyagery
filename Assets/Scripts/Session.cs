using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;

public class MultiChoiceSession
{ 
    // static MultiChoiceSession instance;

    // void Awake()
    // {
    //     if (!instance) {
    //         instance = new MultiChoiceSession();
    //     }
    //     Debug.Log("Awake method: Initializing something on game start");
    // }

    private List<MultiChoice> Questions;
    private int _CurrrentQuestion = -1;
    private int NumCorrect = 0;

    public MultiChoice NextQuestion {
        get {
            if (_CurrrentQuestion + 1 >= Questions.Count) {
                Debug.LogError("Already reached the last question");
            }
            return Questions[++_CurrrentQuestion];
        }
    }

    public MultiChoice CurrentQuestion {
        get {
            if (_CurrrentQuestion < 0) {
                Debug.LogError("Getting current question at index -1");
                return null;
            }
            return Questions[_CurrrentQuestion];
        }
    }

    public string CurrentScore {
        get {
            return NumCorrect + "/" + Questions.Count;
        }
    }

    public int NumQuestions {
        get {
            return Questions.Count;
        }
    }


    /// <summary>
    /// Load a multiple choice question set from data base
    /// </summary>
    /// <param name="questionSetID">index of the question set to load</param>
    public MultiChoiceSession(int questionSetID) {
        if (questionSetID >= MultiChoice.QuestionSets.Count) {
            Debug.Log("Question set index out of bound.");
            return;
        }

        Questions = new List<MultiChoice>();

        MultiChoice.QuestionSet qs = MultiChoice.QuestionSets[questionSetID];
        for (int i = 0; i < qs.Descriptions.Count; ++i) {
            List<(string, string)> options = new List<(string, string)>();
            for (int j = 0; j < qs.Options[i].Count; ++j) {
                options.Add((Tag.MultiChoice + i + "-" + Tag.Option + j, qs.Options[i][j]));
            }
            
            Questions.Add(new MultiChoice(Tag.MultiChoice + i, qs.Descriptions[i], options, qs.Answers[i]));
        }

        
    }

    /// <summary>
    /// Submit answer for the current question
    /// </summary>
    /// <param name="optionID">ID of the chosen option</param>
    public bool SubmitAnswer(string optionID) {
        if (_CurrrentQuestion <= 1) {
            return true;
        }
        string pattern = Tag.MultiChoice + _CurrrentQuestion;
        MatchCollection matches = Regex.Matches(optionID, pattern);
        if (matches.Count == 0) {
            Debug.LogError("Miss matching question ID");
            return false;
        }
        pattern = Tag.Option + @"\d+";
        matches = Regex.Matches(optionID, pattern);
        if (matches.Count == 0) {
            Debug.LogError("Miss matching option ID");
            return false;
        }
        int _optionID = int.Parse(matches[0].Value.Substring(Tag.Option.Length));
        if (_optionID == Questions[_CurrrentQuestion].Answer) {
            NumCorrect++;
            return true;
        } 
        return false;
    }

    public bool HasNextQuestion() {
        return (Questions.Count > 0) && (_CurrrentQuestion < Questions.Count - 1);
    }
}
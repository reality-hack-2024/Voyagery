using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    [SerializeField] private TextMeshPro title;
    [SerializeField] private TextMeshPro description;
    [SerializeField] private TextMeshPro sentence1;
    [SerializeField] private TextMeshPro sentence2;
    [SerializeField] private TextMeshPro sentence3;
    [SerializeField] private TextMeshPro sentence4;
    [SerializeField] private TextMeshPro sentence5;
    [SerializeField] private TextMeshPro buttonText;

    public void Press()
    {
        if (title.enabled) {
            title.enabled = false;
            description.enabled = false;
            buttonText.text = "Next";
            sentence1.gameObject.SetActive(true);
        }
        else if (sentence1.enabled) {
            sentence1.enabled = false;
            sentence2.gameObject.SetActive(true);
        }
        else if (sentence2.enabled) {
            sentence2.enabled = false;
            sentence3.gameObject.SetActive(true);
        }
        else if (sentence3.enabled) {
            sentence3.enabled = false;
            sentence4.gameObject.SetActive(true);
        }
        else if (sentence4.enabled) {
            sentence4.enabled = false;
            sentence5.gameObject.SetActive(true);
        }
        else if (sentence5.enabled) {
            sentence5.gameObject.SetActive(true);
            SceneManager.LoadScene(1);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}

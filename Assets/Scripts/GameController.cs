using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class IntroData {
    public static MultipleChoice Q1 = new MultipleChoice{
        Description = "Why do you seek citizenship in the United States of America?\n A. I want to stay here for work\n B. I plan to attend school here\n C. I want to reunite with family",
        NumOptions = 3,
        Responses = new List<string> {
            "Ah, work. Mankind’s eternal burden.",
            "Academia nourishes minds and helps to create the next generation of leaders and innovators.",
            "The bond between kin is often unshakeable. No distance can keep family apart.",
        }
    };

    public static MultipleChoice Q2 = new MultipleChoice{
        Description = "Noted, thank you for letting me know. Let’s move onto the next question: Tell me about your education and work experience. What is your occupation or unique skillset? Grab your most familiar tool!",
        NumOptions = 3,
        Responses = new List<string> {
            "A game designer? Interesting. Do you ever think that maybe we are in a game?",
            "Impressive! Painting is the finest of all arts!",
            "A doctor? I bet you make a lot of money!"
        }
    };
}

public class MCData {
    public static List<MultipleChoice> Questions = new List<MultipleChoice> {
        new MultipleChoice{
            Description = "Question 1: What is the highest court in the United States?\n A. The Supreme Court\n B. The Court of Justice\n C. The Court of Public Opinion\n D. The People’s Court",
            NumOptions = 4,
            Answer = 0,
            Responses = new List<string> {
                "Correct.",
                "Incorrect, the correct answer should be A. The Supreme Court.",
            }
        },
        new MultipleChoice{
            Description = "Question 2: What is the name of the U.S. war between the North and the South?\n A. The Civil War\n B. The Three Days’ War\n C. The War of Independence\n D. The War of 1800",
            NumOptions = 4,
            Answer = 0,
            Responses = new List<string> {
                "Correct.",
                "Incorrect, the correct answer should be A. The Civil War.",
            }
        },
        new MultipleChoice{
            Description = "Question 3: What is the United States’ national anthem?\n A. The Star-Spangled Banner\n B. Sweet Home Alabama\n C. Home of the Brave\n D. Amber Waves of Grain",
            NumOptions = 4,
            Answer = 0,
            Responses = new List<string> {
                "Correct.",
                "Incorrect, the correct answer should be A. The Star-Spangled Banner.",
            }
        },
        new MultipleChoice{
            Description = "Question 4: Who does a U.S. senator represent?\n A. The people who reside in their state\n B. Special interests\n C. Themselves\n D.  The United States Senate",
            NumOptions = 4,
            Answer = 0,
            Responses = new List<string> {
                "Correct.",
                "Incorrect, the correct answer should be ",
            }
        },
        new MultipleChoice{
            Description = "Question 1: What is the highest court in the United States?\n A. The Supreme Court\n B. The Court of Justice\n C. The Court of Public Opinion\n D. The People’s Court",
            NumOptions = 4,
            Answer = 0,
            Responses = new List<string> {
                "Correct.",
                "Incorrect, the correct answer should be ",
            }
        },
        new MultipleChoice{
            Description = "Question 1: What is the highest court in the United States?\n A. The Supreme Court\n B. The Court of Justice\n C. The Court of Public Opinion\n D. The People’s Court",
            NumOptions = 4,
            Answer = 0,
            Responses = new List<string> {
                "Correct.",
                "Incorrect, the correct answer should be ",
            }
        }
    };
}

public enum State {
    Init,
    IntroQ1,
    IntroQ2,
    MC,
    End,
}


public class GameController : MonoBehaviour
{
    State state = State.Init;
    int CurrentMC = 0;

    void Start() {
        state = State.IntroQ1;
        IntroQ1();
    }

    void IntroQ1() {   
        // question 1
        SetQuestionText(IntroData.Q1.Description);
    }

    void IntroQ2(string response) {
        string question = response + "\n\n" + IntroData.Q2.Description;
        SetQuestionText(question);
    }

    void MC(string response) {
        string question = (response == null ? "" : response + "\n\n") + MCData.Questions[CurrentMC].Description;
        SetQuestionText(question);
    }

    void SetQuestionText(string text) {
        Transform Question = transform.Find("question");
        if (Question != null) {
            Question.GetComponent<TextMeshPro>().text = text;
        } else {
            Debug.LogError("question object not found");
        }
    }

    public void SubmitOption(string optionID) {
        int index = int.Parse(optionID[^1..]);
        switch(state) {
            case State.IntroQ1:
                state = State.IntroQ2;
                IntroQ2(IntroData.Q1.Responses[index]);
                break;
            case State.IntroQ2:
                state = State.MC;
                MC(null);
                break;
            case State.MC:
                string response = MCData.Questions[CurrentMC].Responses[MCData.Questions[CurrentMC].Answer == index ? 0 : 1];
                CurrentMC++;
                if (CurrentMC == MCData.Questions.Count) {
                    // last question
                    state = State.End;
                } else {
                    MC(response);
                }
                break;
            case State.End:
                break;
        }
    }
}

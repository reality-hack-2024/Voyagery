using System.Collections.Generic;
using UnityEngine;



public class MultiChoice
{
    public class QuestionSet {
        public List<string> Descriptions;
        public List<List<string>> Options;
        public List<int> Answers;
    }

    // hard-coded in-memory question set database
    static public readonly List<QuestionSet> QuestionSets = new List<QuestionSet>();
    static public void LoadQuestions() {
        QuestionSet qs = new QuestionSet
        {
            Descriptions = new List<string>{
            "Why do you seek citizenship in the United States of America?",
            "Noted, thank you for letting me know. Let’s move onto the next question: Tell me about your education and work experience. What is your occupation or unique skillset?",
            "What is the highest court in the United States?",
            "What is the name of the U.S. war between the North and the South?",
            "What is the United States’ national anthem?",
            "How is one branch of government prevented from becoming too powerful?",
            "Who does a U.S. senator represent?",
            "When is Independence Day?",
            "When was the constitution written?",
            // "The idea of self-government is in the first three words of the Constitution. What are these words?",
            // "What are two rights in the Declaration of Independence?",
        },

            Options = new List<List<string>>{
            new List<string>{"Work", "School", "Family", "Friends"},
            new List<string>{"Chef", "Game Dev", "Painter", "Doctor"},
            new List<string>{"Supreme Court", "Court of Justice", "Court of Public Opinion", "The People's Court"},
            new List<string>{"The Three Days' War", "The Civil War", "The War of Independence", "The War of 1800"},
            new List<string>{"Home of the Brave", "Sweet Home Alabama", "The Star-Spangled Banner", "Amber Waves of Grain"},
            new List<string>{"Filibuster", "Checks and Balances", "Diplomatic Immunity", "The People"},
            new List<string>{"the United States Senate", "special interests", "themselves", "State residents"},
            new List<string>{"4th of July", "25th of December", "1st of April", "15th of March"},
            new List<string>{"1799", "1812", "1787", "1519"},
            // new List<string>{"People", "We", "The", ""},
            // new List<string>{"Of", "Bill", "Rights", ""},
        },

            Answers = new List<int> { 0, 0, 0, 1, 2, 1, 3, 0, 2, 3 }
        };

        QuestionSets.Add(qs);

    }

    public string ID;
    public string Description;
    public List<(string, string)> Options;
    public int Answer;

    public MultiChoice(string id, string description, List<(string, string)> options, int answer) {
        ID = id;
        Description = description;
        Options = options;
        Answer = answer;
    }
}

public class MultipleChoice {
    public string Description;
    public int NumOptions;
    public int Answer;
    public List<string> Responses;
}

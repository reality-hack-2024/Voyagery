using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using TMPro;

public class AnswerTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private WordPuzzleGame2 wordPuzzle;
    [SerializeField] private TextMeshPro currentText;

    void OnTriggerEnter(Collider col)
    {
        if (col.name == "Trigger1") {
            wordPuzzle.modifyAnswer(0, currentText.text);
        }
        else if (col.name == "Trigger2") {
            wordPuzzle.modifyAnswer(1, currentText.text);
        }
        else if (col.name == "Trigger3") {
            wordPuzzle.modifyAnswer(2, currentText.text);
        }


        // GetComponent<Collider>().enabled = false;
    }
}

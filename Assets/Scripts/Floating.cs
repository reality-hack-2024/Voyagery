using UnityEngine;

public class Floating : MonoBehaviour
{
    public float floatSpeed = 1.0f; // Adjust the floating speed in the Inspector
    public float floatHeight = 0.01f; // Adjust the floating height in the Inspector

    private Vector3 initialPosition;

    public GameObject GameControllerObject;

    public GameObject Capture;

    void Start()
    {
        // Save the initial position for reference
        initialPosition = transform.position;
    }

    void Update()
    {
        // Calculate the new Y position based on sine function
        float newY = initialPosition.y + Mathf.Sin(Time.time * floatSpeed) * floatHeight;

        // Update the object's position
        transform.position = new Vector3(transform.position.x, newY, transform.position.z);
        if (Capture != null) {
            Capture.transform.position = gameObject.transform.position;
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag[..2] == "op") {
            Debug.Log("User selected option with ID: " + other.gameObject.tag);
            Capture = other.gameObject;
            GameControllerObject.GetComponent<GameController>().SubmitOption(other.gameObject.tag);
        }
        Debug.Log("other trigger?????");
    }
 }

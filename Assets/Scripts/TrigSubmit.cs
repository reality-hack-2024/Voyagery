using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Oculus.Interaction
{
    public class TrigSubmit : MonoBehaviour
{
    public GameObject GameControllerObject;

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.name == "dummy-hand") {
            Debug.Log("User selected option with ID: " + gameObject.name);
            GameControllerObject.GetComponent<GameController>().SubmitOption(gameObject.name);
        }
    }

    public void OnSelected() {
        Debug.Log("User selected option with ID: " + gameObject.name);
        GameControllerObject.GetComponent<GameController>().SubmitOption(gameObject.name);
    }
}
}

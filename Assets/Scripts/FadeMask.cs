using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FadeMask : MonoBehaviour
{
    private float desiredAlpha = 0f;
    private float currentAlpha = 1f;

    public float speed = 0.5f;
    public bool isTriggered = true;
    private float lerpParam;
    private float startAlpha;

    void Update()
    {
        if (isTriggered){
            currentAlpha = Mathf.MoveTowards(currentAlpha, desiredAlpha, Time.deltaTime*speed);
            this.GetComponent<RawImage>().color = new Color(0, 0, 0, currentAlpha);
            if (currentAlpha == desiredAlpha){
                isTriggered = false;
                if (currentAlpha == 1){
                    desiredAlpha = 0;
                }else if(currentAlpha == 0){
                    desiredAlpha = 1;
                }
            }else{
                isTriggered = true;
            }
        }
    }
}


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DisplayText : MonoBehaviour
{
    private float time = 0;
    private int question = -1;
    private List<string> data = new List<string>{
        "Hello, please close the door and take a seat.",
        "Welcome to the United States’ immigration visa interview. During this interview, you will be asked 10 civics questions as part of the naturalization test for obtaining naturalized citizenship to the United States. Before that, I am also required to ask you a number of personal questions in order to understand why you seek United States citizenship.",
        "Please understand that none of these questions will be used for discriminatory purposes, and answers will be kept confidential. Please answer honestly and fully. Any false or misleading information is punishable by law. Are you ready to begin?",
        "...",
        "...",
        "Interesting indeed. Please recognize that all able-bodied citizens are expected to maintain consistent employment or stable income  and to keep up with all financial obligations. Utilization of public welfare services should be temporary and only occur in circumstances of great need.",
        "Next, we will test your knowledge of American civics and government. We will ask 10 random questions from a pool of 100, and you must answer at least 6 of them correctly to pass the exam. Think carefully before answering.",
        "...",
        "...",
        "...",
        "...",
        "...",
        "...",
        "...",
        "Puzzle",
        "Puzzle",
        "Puzzle",
    };
    // [SerializeField] private TextMeshPro textDisplay;
    [SerializeField] private GameObject multichoice;
    [SerializeField] private GameObject nextButton;
    [SerializeField] private GameObject puzzle;
  
    // Start is called before the first frame update
    void Start()
    {
        displayNextQuestion();
    }

    public void displayNextQuestion() {
        question++;
        if (question == data.Count) {
            // end game
            return;
        }
        if (data[question] == "...") {
            multichoice.GetComponent<MultipleChoiceQuestions>().nextQuestion();
            return;
        }
        if (data[question] == "Puzzle") {
            puzzle.GetComponent<WordPuzzleGame2>().nextWordPuzzle();
            return;
        }
        GetComponent<TextMeshPro>().text = data[question];
        nextButton.SetActive(true);
    }
}

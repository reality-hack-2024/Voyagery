using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MultipleChoiceQuestions : MonoBehaviour
{
    private static MultiChoiceSession session;
    [SerializeField] private TextMeshPro answer1;
    [SerializeField] private TextMeshPro answer2;
    [SerializeField] private TextMeshPro answer3;
    [SerializeField] private TextMeshPro answer4;
    [SerializeField] private TextMeshPro question;
    [SerializeField] private TextMeshPro feedback;
    [SerializeField] private GameObject nextButton;
    // Start is called before the first frame update
    void Start()
    {
        MultiChoice.LoadQuestions();
        session = new MultiChoiceSession(0);
    }

    private void beginQuiz() {
        
    }


    public void nextQuestion()
    {
        nextButton.SetActive(false);
        MultiChoice mc = session.NextQuestion;
        question.text = mc.Description;
        answer1.text = mc.Options[0].Item2;
        answer1.name = mc.Options[0].Item1;
        answer2.text = mc.Options[1].Item2;
        answer2.name = mc.Options[1].Item1;
        answer3.text = mc.Options[2].Item2;
        answer3.name = mc.Options[2].Item1;
        answer4.text = mc.Options[3].Item2;
        answer4.name = mc.Options[3].Item1;
        
        answer1.transform.parent.gameObject.SetActive(true);
        answer2.transform.parent.gameObject.SetActive(true);
        answer3.transform.parent.gameObject.SetActive(true);
        answer4.transform.parent.gameObject.SetActive(true);
    }
    public void submitAnswer(TextMeshPro answer) {
        if (session.SubmitAnswer(answer.name)) {
            answer1.transform.parent.gameObject.SetActive(false);
            answer2.transform.parent.gameObject.SetActive(false);
            answer3.transform.parent.gameObject.SetActive(false);
            answer4.transform.parent.gameObject.SetActive(false);
            feedback.gameObject.SetActive(false);
            // feedback.gameObject.SetActive(false);
            question.GetComponent<DisplayText>().displayNextQuestion();
        }
        else {
            feedback.gameObject.SetActive(true);
        }
    }
}
